import time

from appium import webdriver

class Anotherfile:
    def another(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['automationName'] = 'UiAutomator2'
        desired_caps['platformVersion'] = '10'
        desired_caps['deviceName'] = 'Pixel 3a XL API 29'
        desired_caps['app'] = ('C:\\Users\\Trishita.Moharar\\Downloads\\app-debug-26-apr.apk')
        desired_caps['appPackage'] = 'com.edio'
        desired_caps['appActivity'] = 'com.edio.MainActivity'
        desired_caps['noReset'] = 'true'

        driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desired_caps)
        time.sleep(20)
        driver.find_element_by_accessibility_id("absenceschecklater").click()
        time.sleep(5)
        driver.find_element_by_accessibility_id("More, tab, 4 of 4").click()
        time.sleep(2)
        driver.find_element_by_accessibility_id("logoutMenu").click()