import glob
import os



# prepare something ahead of all tests
path = os.getcwd()
print(path)
files = glob.glob(os.path.join('C:\\Users\\Trishita.Moharar\\.jenkins\\workspace\\EdioGo\\Test\\Testcases',"*.png"))
filename = glob.glob(os.path.join('C:\\Users\\Trishita.Moharar\\.jenkins\\workspace\\EdioGo\\',"logfile.log"))
filejson = glob.glob(os.path.join('C:\\Users\\Trishita.Moharar\\.jenkins\\workspace\\EdioGo\\',"sample.json"))
for f in files:

    ## If file exists, delete it ##
    if os.path.exists(f):
        os.remove(f)
    else:  ## Show an error ##
        print("No error file present")

for f in filename:

    ## If file exists, delete it ##
    if os.path.exists(f):
        os.remove(f)
    else:  ## Show an error ##
        print("No logfile file present")


for f in filejson:

    ## If file exists, delete it ##
    if os.path.exists(f):
        os.remove(f)
    else:  ## Show an error ##
        print("No sample json file present")

# dir = 'path/to/dir'
# filelist = glob.glob(os.path.join(dir, "*.png"))
# for f in filelist:
#     os.remove(f)
