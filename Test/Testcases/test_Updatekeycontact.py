import time

import pytest

from config import config
from pageobject.Dashboard import Dashboard
from pageobject.Help import Help
from pageobject.Calender import Calender
from pageobject.UI_Updates import UI_Updates
from pageobject.Uupdates_keycontact import Updates_keycontact
from utilities.Base import Baseclass
from pytest_html_reporter import attach


class Testcaseupdatekeycontact(Baseclass):



    def test_testcase421(self, setup, test_failed_check):
        logger = self.getLogger()
        uicontact = Updates_keycontact(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase421")
        uicontact.case421()
        logger.info("ended testcase421")


    def test_testcase422(self, setup, test_failed_check):
        logger = self.getLogger()
        uicontact = Updates_keycontact(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase422")
        uicontact.case422()
        logger.info("ended testcase422")


    def test_testcase423(self, setup, test_failed_check):
        logger = self.getLogger()
        uicontact = Updates_keycontact(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase423")
        uicontact.case423()
        logger.info("ended testcase423")


    def test_testcase424(self, setup, test_failed_check):
        logger = self.getLogger()
        uicontact = Updates_keycontact(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase424")
        uicontact.case424()
        logger.info("ended testcase424")


    def test_testcase430(self, setup, test_failed_check):
        logger = self.getLogger()
        uicontact = Updates_keycontact(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase430")
        uicontact.case430()
        logger.info("ended testcase430")


