import time

import pytest

from config import config
from pageobject.Dashboard import Dashboard
from pageobject.Help import Help
from utilities.Base import Baseclass
from pytest_html_reporter import attach


class TestcaseHelp(Baseclass):



    def test_testcase202(self, setup, test_failed_check):
        logger = self.getLogger()
        help = Help(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase202")
        help.case202()
        logger.info("ended testcase202")

    def test_testcase203(self, setup, test_failed_check):
        logger = self.getLogger()
        help = Help(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase203")
        help.case203()
        logger.info("ended testcase203")

    # @pytest.mark.skip(reason="no way of currently testing this")
    def test_testcase204(self, setup, test_failed_check):
        logger = self.getLogger()
        help = Help(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase204")
        help.case204()
        logger.info("ended testcase204")

    def test_testcase205(self, setup, test_failed_check):
        logger = self.getLogger()
        help = Help(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase205")
        help.case205()
        logger.info("ended testcase205")

    def test_testcase206(self, setup, test_failed_check):
        logger = self.getLogger()
        help = Help(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase206")
        help.case206()
        logger.info("ended testcase206")

    def test_testcase192(self, setup, test_failed_check):
        logger = self.getLogger()
        help = Help(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase192")
        help.case192()
        logger.info("ended testcase192")


    def test_testcase280(self, setup, test_failed_check):
        logger = self.getLogger()
        help = Help(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase280")
        help.case280()
        logger.info("ended testcase280")























