import time

import pytest

from config import config
from pageobject.Dashboard import Dashboard
from pageobject.Feedbackmechanism import Feedback
from pageobject.Gradebook import Gradebook
from pageobject.Help import Help
from pageobject.chatoption import Chatoption
from utilities.Base import Baseclass
from pytest_html_reporter import attach


class TestcaseChatoption(Baseclass):



    def test_testcase391(self, setup, test_failed_check):
        logger = self.getLogger()
        chat = Chatoption(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase391")
        chat.case391()
        logger.info("ended testcase391")


    def test_testcase398(self, setup, test_failed_check):
        logger = self.getLogger()
        chat = Chatoption(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase398")
        chat.case398()
        logger.info("ended testcase398")

    def test_testcase412(self, setup, test_failed_check):
        logger = self.getLogger()
        chat = Chatoption(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase412")
        chat.case412()
        logger.info("ended testcase412")

    def test_testcase415(self, setup, test_failed_check):
        logger = self.getLogger()
        chat = Chatoption(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase415")
        chat.case415()
        logger.info("ended testcase415")

