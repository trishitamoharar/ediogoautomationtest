import time

import pytest

from config import config
from pageobject.Dashboard import Dashboard
from pageobject.Help import Help
from pageobject.More import More
from pageobject.OverdueItem import OverdueItem
from utilities.Base import Baseclass
from pytest_html_reporter import attach


class TestcaseOverdueitem(Baseclass):



    def test_testcase66(self, setup, test_failed_check):
        logger = self.getLogger()
        overdue = OverdueItem(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase66")
        overdue.case66()
        logger.info("ended testcase66")

    def test_testcase287(self, setup, test_failed_check):
        logger = self.getLogger()
        overdue = OverdueItem(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase287")
        overdue.case287()
        logger.info("ended testcase287")

    def test_testcase373(self, setup, test_failed_check):
        logger = self.getLogger()
        overdue = OverdueItem(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase373")
        overdue.case373()
        logger.info("ended testcase373")

    def test_testcase380(self, setup, test_failed_check):
        logger = self.getLogger()
        overdue = OverdueItem(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase380")
        overdue.case380()
        logger.info("ended testcase380")

