import time

import pytest

from config import config
from pageobject.Dashboard import Dashboard
from pageobject.Help import Help
from pageobject.Calender import Calender
from utilities.Base import Baseclass
from pytest_html_reporter import attach


class TestcaseCalender(Baseclass):



    def test_testcase122(self, setup, test_failed_check):
        logger = self.getLogger()
        calender = Calender(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase122")
        calender.case122()
        logger.info("ended testcase122")

    def test_testcase128(self, setup, test_failed_check):
        logger = self.getLogger()
        calender = Calender(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase128")
        calender.case128()
        logger.info("ended testcase128")

    def test_testcase134(self, setup, test_failed_check):
        logger = self.getLogger()
        calender = Calender(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase134")
        calender.case134()
        logger.info("ended testcase134")

    def test_testcase131(self, setup, test_failed_check):
        logger = self.getLogger()
        calender = Calender(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase131")
        calender.case131()
        logger.info("ended testcase131")
