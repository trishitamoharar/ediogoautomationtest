import time

import pytest

from config import config
from pageobject.Dashboard import Dashboard
from pageobject.Feedbackmechanism import Feedback
from pageobject.Gradebook import Gradebook
from pageobject.Help import Help
from utilities.Base import Baseclass
from pytest_html_reporter import attach


class TestcaseGradebook(Baseclass):



    def test_testcase433(self, setup, test_failed_check):
        logger = self.getLogger()
        gradebook = Gradebook(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase433")
        gradebook.case433()
        logger.info("ended testcase433")

    def test_testcase355(self, setup, test_failed_check):
        logger = self.getLogger()
        gradebook = Gradebook(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase355")
        gradebook.case355()
        logger.info("ended testcase355")

    def test_testcase358(self, setup, test_failed_check):
        logger = self.getLogger()
        gradebook = Gradebook(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase358")
        gradebook.case358()
        logger.info("ended testcase358")


    def test_testcase365(self, setup, test_failed_check):
        logger = self.getLogger()
        gradebook = Gradebook(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase365")
        gradebook.case365()
        logger.info("ended testcase365")



