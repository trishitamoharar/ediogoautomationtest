import time

import pytest

from config import config
from pageobject.Dashboard import Dashboard
from pageobject.Feedbackmechanism import Feedback
from pageobject.Help import Help
from utilities.Base import Baseclass
from pytest_html_reporter import attach


class TestcaseFeedback(Baseclass):



    def test_testcase82(self, setup, test_failed_check):
        logger = self.getLogger()
        feedback = Feedback(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase82")
        feedback.case82()
        logger.info("ended testcase82")