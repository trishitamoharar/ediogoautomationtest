import time

import pytest

from config import config
from pageobject.Dashboard import Dashboard
from utilities.Base import Baseclass
from pytest_html_reporter import attach


class TestcaseDashboard(Baseclass):



    def test_testcase8(self, setup, test_failed_check):
        logger = self.getLogger()
        dashboard = Dashboard(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase8")
        dashboard.case8()
        logger.info("ended testcase8")

    def test_testcase9(self, setup, test_failed_check):
        logger = self.getLogger()
        dashboard = Dashboard(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase9")
        dashboard.case9()
        logger.info("ended testcase9")

    def test_testcase10(self, setup, test_failed_check):
        logger = self.getLogger()
        dashboard = Dashboard(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase10")
        dashboard.case10()
        logger.info("ended testcase10")

    def test_testcase11(self, setup, test_failed_check):
        logger = self.getLogger()
        dashboard = Dashboard(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase11")
        dashboard.case11()
        logger.info("ended testcase11")

    def test_testcase12(self, setup, test_failed_check):
        logger = self.getLogger()
        dashboard = Dashboard(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase12")
        dashboard.case12()
        logger.info("ended testcase12")


    #@pytest.mark.skip(reason="no way of currently testing this")
    def test_testcase23(self, setup, test_failed_check):
        logger = self.getLogger()
        dashboard = Dashboard(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase23")
        dashboard.case23()
        logger.info("ended testcase23")

    def test_testcase192(self, setup, test_failed_check):
        logger = self.getLogger()
        dashboard = Dashboard(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase192")
        dashboard.case192()
        logger.info("ended testcase192")

    def test_testcase194(self, setup, test_failed_check):
        logger = self.getLogger()
        dashboard = Dashboard(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase194")
        dashboard.case194()
        logger.info("ended testcase194")

    def test_testcase195(self, setup, test_failed_check):
        logger = self.getLogger()
        dashboard = Dashboard(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase195")
        dashboard.case195()
        logger.info("ended testcase195")

















