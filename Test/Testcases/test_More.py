import time

import pytest

from config import config
from pageobject.Dashboard import Dashboard
from pageobject.Help import Help
from pageobject.More import More
from utilities.Base import Baseclass
from pytest_html_reporter import attach


class TestcaseMore(Baseclass):



    def test_testcase53(self, setup, test_failed_check):
        logger = self.getLogger()
        more = More(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase53")
        more.case53()
        logger.info("ended testcase53")

    def test_testcase174(self, setup, test_failed_check):
        logger = self.getLogger()
        more = More(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase174")
        more.case174()
        logger.info("ended testcase174")


    def test_testcase176(self, setup, test_failed_check):
        logger = self.getLogger()
        more = More(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase176")
        more.case176()
        logger.info("ended testcase176")

    def test_testcase185(self, setup, test_failed_check):
        logger = self.getLogger()
        more = More(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase185")
        more.case185()
        logger.info("ended testcase185")


    def test_testcase327(self, setup, test_failed_check):
        logger = self.getLogger()
        more = More(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase327")
        more.case327()
        logger.info("ended testcase327")

    def test_testcase329(self, setup, test_failed_check):
        logger = self.getLogger()
        more = More(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase329")
        more.case329()
        logger.info("ended testcase329")

    def test_testcase330(self, setup, test_failed_check):
        logger = self.getLogger()
        more = More(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase330")
        more.case330()
        logger.info("ended testcase330")
