import time

import pytest

from config import config
from pageobject.Dashboard import Dashboard
from pageobject.Help import Help
from pageobject.Calender import Calender
from pageobject.UI_Updates import UI_Updates
from utilities.Base import Baseclass
from pytest_html_reporter import attach


class TestcaseUiupdates(Baseclass):



    def test_testcase335(self, setup, test_failed_check):
        logger = self.getLogger()
        uiupdates = UI_Updates(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase335")
        uiupdates.case335()
        logger.info("ended testcase335")


    def test_testcase317(self, setup, test_failed_check):
        logger = self.getLogger()
        uiupdates = UI_Updates(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase317")
        uiupdates.case317()
        logger.info("ended testcase317")

    def test_testcase318(self, setup, test_failed_check):
        logger = self.getLogger()
        uiupdates = UI_Updates(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase318")
        uiupdates.case318()
        logger.info("ended testcase318")


    def test_testcase319(self, setup, test_failed_check):
        logger = self.getLogger()
        uiupdates = UI_Updates(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase319")
        uiupdates.case319()
        logger.info("ended testcase319")


    def test_testcase320(self, setup, test_failed_check):
        logger = self.getLogger()
        uiupdates = UI_Updates(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase320")
        uiupdates.case320()
        logger.info("ended testcase320")


    def test_testcase321(self, setup, test_failed_check):
        logger = self.getLogger()
        uiupdates = UI_Updates(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase321")
        uiupdates.case321()
        logger.info("ended testcase321")


    def test_testcase324(self, setup, test_failed_check):
        logger = self.getLogger()
        uiupdates = UI_Updates(self.driver)
        conf = config()
        time.sleep(5)
        logger.info("started testcase324")
        uiupdates.case324()
        logger.info("ended testcase324")


