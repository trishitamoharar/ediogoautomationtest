import os
import time
from datetime import datetime

import browserstack as browserstack
import xlrd
from appium import webdriver
import pytest
from browserstack.local import Local
import pytest_html
from numpy import append
from pytest_html_reporter import attach
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from config import config

driver = None


# path = os.path.dirname(os.getcwd())


@pytest.fixture(scope="function")
def setup(request):
    global driver
    #####################################browserstack local##################
    # creates an instance of Local
    # bs_local = Local()
    #
    # # replace <browserstack-accesskey> with your key. You can also set an environment variable - "BROWSERSTACK_ACCESS_KEY".
    # bs_local_args = {"key": "<browserstack-accesskey>"}
    #
    # # starts the Local instance with the required arguments
    # bs_local.start(**bs_local_args)
    #
    # # check if BrowserStack local instance is running
    # print(bs_local.isRunning())
    #
    # bs_local_args = {"key": "<browserstack-accesskey>", "forcelocal": "true"}
    #
    # # stop the Local instance
    # bs_local.stop()
    #########################################

    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['automationName'] = 'UiAutomator2'
    desired_caps['platformVersion'] = '10'
    desired_caps['deviceName'] = 'Pixel 3a XL API 29'
    desired_caps['app'] = ('C:\\Users\\Trishita.Moharar\\Downloads\\edio_17th_Aug_Dev_1.apk')
    desired_caps['appPackage'] = 'com.edio'
    desired_caps['appActivity'] = 'com.edio.MainActivity'
    desired_caps['noReset'] = 'true'
    desired_caps['unicodeKeyboard'] = 'true'
    desired_caps['resetKeyboard'] = 'true'

    driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desired_caps)

    #  desired_cap = {
    #      # Set your access credentials
    #      "browserstack.user": "trishitamoharar1",
    #      "browserstack.key": "mFwLdTDgQpx2BYMMf9Ex",
    #
    #      # Set URL of the application under test
    #      "app": "bs://247c4d08d10cb31dd881ac2f7872b6e1faef83c8",
    #
    #      # Specify device and os_version for testing
    #      "device": "Google Pixel 3",
    #      "os_version": "9.0",
    #
    #      # Set other BrowserStack capabilities
    #      "project": "First Python project",
    #      "build": "Python Android",
    #      "name": "first_test",
    #      "noReset": "true"
    #  }
    #
    # # Initialize the remote Webdriver using BrowserStack remote URL
    #  #and desired capabilities defined above
    #  driver = webdriver.Remote(
    #      command_executor="http://hub-cloud.browserstack.com/wd/hub",
    #      desired_capabilities=desired_cap
    #  )
    time.sleep(20)
    # driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='nexttologin']/android.widget.ImageView").click()
    # time.sleep(3)
    # progressexpected = "CHECK PROGRESS"
    # progressactual = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.HorizontalScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
    # assert progressexpected == progressactual
    # driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='nexttologin']/android.widget.ImageView").click()
    # time.sleep(3)
    # display = driver.find_element_by_class_name("android.widget.ImageView").is_displayed()
    # assert display == "True"
    # driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView")
    # print("The Edio Go logo is present")
    driver.find_element_by_accessibility_id("usernameinput").send_keys("cgolden7773")
    driver.find_element_by_accessibility_id("usernameinputinput").send_keys("Passw0rd!")
    # time.sleep(4)
    # checkvalue = driver.find_element_by_xpath(
    #     "//android.widget.CheckBox[@content-desc='keepmelogin']/android.widget.ImageView").get_attribute("checked")
    #
    # assert checkvalue == "false"
    driver.find_element_by_accessibility_id("loginbutton").click()
    time.sleep(25)
    # btn = driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='loginbutton'")
    # driver.execute_script("arguments[0].click();", btn)
    # driver.find_element_by_xpath("//android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView").click()
    # time.sleep(5)
    driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]").click()
    time.sleep(3)
    # except:
    #     print("The unexcused popup is not present")
    request.cls.driver = driver
    yield

    # driver.quit()


@pytest.fixture(scope="function")
def setuppastdue(request):
    global driver

    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['automationName'] = 'UiAutomator2'
    desired_caps['platformVersion'] = '10'
    desired_caps['deviceName'] = 'Pixel 3a XL API 29'
    desired_caps['app'] = ('C:\\Users\\Trishita.Moharar\\Downloads\\edio_5th_Aug_CCA.apk')
    desired_caps['appPackage'] = 'com.edio'
    desired_caps['appActivity'] = 'com.edio.MainActivity'
    desired_caps['noReset'] = 'true'
    desired_caps['unicodeKeyboard'] = 'true'
    desired_caps['resetKeyboard'] = 'true'

    driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desired_caps)

    #  desired_cap = {
    #      # Set your access credentials
    #      "browserstack.user": "trishitamoharar1",
    #      "browserstack.key": "mFwLdTDgQpx2BYMMf9Ex",
    #
    #      # Set URL of the application under test
    #      "app": "bs://247c4d08d10cb31dd881ac2f7872b6e1faef83c8",
    #
    #      # Specify device and os_version for testing
    #      "device": "Google Pixel 3",
    #      "os_version": "9.0",
    #
    #      # Set other BrowserStack capabilities
    #      "project": "First Python project",
    #      "build": "Python Android",
    #      "name": "first_test",
    #      "noReset": "true"
    #  }
    #
    # # Initialize the remote Webdriver using BrowserStack remote URL
    #  #and desired capabilities defined above
    #  driver = webdriver.Remote(
    #      command_executor="http://hub-cloud.browserstack.com/wd/hub",
    #      desired_capabilities=desired_cap
    #  )
    time.sleep(20)
    # driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='nexttologin']/android.widget.ImageView").click()
    # time.sleep(3)
    # progressexpected = "CHECK PROGRESS"
    # progressactual = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.HorizontalScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
    # assert progressexpected == progressactual
    # driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='nexttologin']/android.widget.ImageView").click()
    # time.sleep(3)
    # display = driver.find_element_by_class_name("android.widget.ImageView").is_displayed()
    # assert display == "True"
    # driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView")
    # print("The Edio Go logo is present")
    driver.find_element_by_accessibility_id("usernameinput").send_keys("rjewell5126")
    driver.find_element_by_accessibility_id("usernameinputinput").send_keys("Passw0rd!")
    # time.sleep(4)
    # checkvalue = driver.find_element_by_xpath(
    #     "//android.widget.CheckBox[@content-desc='keepmelogin']/android.widget.ImageView").get_attribute("checked")
    #
    # assert checkvalue == "false"
    driver.find_element_by_accessibility_id("loginbutton").click()
    time.sleep(15)
    # btn = driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='loginbutton'")
    # driver.execute_script("arguments[0].click();", btn)
    # driver.find_element_by_xpath("//android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView").click()
    # time.sleep(5)
    driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]").click()
    time.sleep(4)
    request.cls.driver = driver
    yield


@pytest.fixture(scope="function")
def setupview(request):
    global driver

    from browserstack.local import Local

    # creates an instance of Local
    bs_local = Local()

    # replace <browserstack-accesskey> with your key. You can also set an environment variable - "BROWSERSTACK_ACCESS_KEY".
    bs_local_args = {"key": "<browserstack-accesskey>"}

    # starts the Local instance with the required arguments
    bs_local.start(**bs_local_args)

    # check if BrowserStack local instance is running
    print(bs_local.isRunning())

    # stop the Local instance
    bs_local.stop()

    # desired_caps = {}
    # desired_caps['platformName'] = 'Android'
    # desired_caps['automationName'] = 'UiAutomator2'
    # desired_caps['platformVersion'] = '10'
    # desired_caps['deviceName'] = 'Pixel 3a XL API 29'
    # desired_caps['app'] = ('C:\\Users\\Trishita.Moharar\\Downloads\\edio_5th_Aug_CCA.apk')
    # desired_caps['appPackage'] = 'com.edio'
    # desired_caps['appActivity'] = 'com.edio.MainActivity'
    # desired_caps['noReset'] = 'true'
    # desired_caps['unicodeKeyboard'] = 'true'
    # desired_caps['resetKeyboard'] = 'true'
    #
    # driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desired_caps)

    # desired_cap = {
    #     # Set your access credentials
    #     "browserstack.user": "trishitamoharar1",
    #     "browserstack.key": "mFwLdTDgQpx2BYMMf9Ex",
    #
    #     # Set URL of the application under test
    #     "app": "bs://247c4d08d10cb31dd881ac2f7872b6e1faef83c8",
    #
    #     # Specify device and os_version for testing
    #     "device": "Google Pixel 3",
    #     "os_version": "9.0",
    #
    #     # Set other BrowserStack capabilities
    #     "project": "First Python project",
    #     "build": "Python Android",
    #     "name": "first_test",
    #     "noReset": "true"
    # }
    #
    #
    #
    # # Initialize the remote Webdriver using BrowserStack remote URL
    #  #and desired capabilities defined above
    #  driver = webdriver.Remote(
    #      command_executor="http://hub-cloud.browserstack.com/wd/hub",
    #      desired_capabilities=desired_cap
    #  )
    time.sleep(15)
    # driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='nexttologin']/android.widget.ImageView").click()
    # time.sleep(3)
    # progressexpected = "CHECK PROGRESS"
    # progressactual = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.HorizontalScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
    # assert progressexpected == progressactual
    # driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='nexttologin']/android.widget.ImageView").click()
    # time.sleep(3)
    # display = driver.find_element_by_class_name("android.widget.ImageView").is_displayed()
    # assert display == "True"
    # driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView")
    # print("The Edio Go logo is present")
    driver.find_element_by_accessibility_id("usernameinput").send_keys("cgolden7773")
    driver.find_element_by_accessibility_id("usernameinputinput").send_keys("Passw0rd!")
    # time.sleep(4)
    # checkvalue = driver.find_element_by_xpath(
    #     "//android.widget.CheckBox[@content-desc='keepmelogin']/android.widget.ImageView").get_attribute("checked")
    #
    # assert checkvalue == "false"
    driver.find_element_by_accessibility_id("loginbutton").click()
    time.sleep(15)
    # btn = driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='loginbutton'")
    # driver.execute_script("arguments[0].click();", btn)
    # driver.find_element_by_xpath("//android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView").click()
    # time.sleep(5)
    driver.find_element_by_xpath(
        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]").click()
    time.sleep(3)
    request.cls.driver = driver
    yield


# set up a hook to be able to check if a test has failed


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    rep = outcome.get_result()
    print(rep)
    print("******************************")
    # set a report attribute for each phase of a call, which can
    # be "setup", "call", "teardown"
    rep.start = call.start
    rep.stop = call.stop
    extra = getattr(rep, 'extra', [])
    setattr(item, "rep_" + rep.when, rep)


# check if a test has failed
@pytest.fixture(scope="function", autouse=True)
def test_failed_check(request):
    yield
    # request.node is an "item" because we use the default
    # "function" scope
    if request.node.rep_setup.failed:
        print("setting up a test failed!", request.node.nodeid)
        # status_browserstack(driver, request)
    elif request.node.rep_setup.passed:
        if request.node.rep_call.failed:
            # driver = request.node.funcargs['selenium_driver']
            take_screenshot(driver, request.node.nodeid)
            print("executing test failed", request.node.nodeid)
    # status_browserstack(driver, request)
    driver.quit()


def status_browserstack(driver, request):
    time.sleep(1)
    if request.node.rep_setup.failed:
        driver.execute_script('browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')
    if request.node.rep_setup.passed:
        if request.node.rep_call.failed:
            # Setting the status of test as 'passed' or 'failed' based on the condition; if title of the web page starts with 'BrowserStack'
            driver.execute_script(
                'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed"}}')
    if request.node.rep_call.passed:
        driver.execute_script('browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed"}}')


# make a screenshot with a name of the test, date and time
def take_screenshot(driver, nodeid):
    time.sleep(1)
    # file_name = "sceenshot.png"
    file_name = nodeid.replace("::", "_") + ".png"
    print(file_name)
    driver.get_screenshot_as_file(file_name)
    driver.save_screenshot(file_name)
    'onclick="window.open(this.src)" align="right"/></div>'


def capture_screenshot(name):
    driver.get_screenshot_as_file(name)


def pytest_terminal_summary(terminalreporter):
    terminalreporter.ensure_newline()
    terminalreporter.section('start/stop times', sep='-', bold=True)
    for stat in terminalreporter.stats.values():
        for report in stat:
            if report.when == 'call':
                start = datetime.fromtimestamp(report.start)
                stop = datetime.fromtimestamp(report.stop)
                terminalreporter.write_line(f'{report.nodeid:20}: {start:%Y-%m-%d %H:%M:%S} - {stop:%Y-%m-%d %H:%M:%S}')


def pytest_json_runtest_metadata(item, call):
    if call.when != 'call':
        return {}
    return {'start': call.start, 'stop': call.stop}

# def pytest_json_runtest_metadata(request):
#     if request.when != 'call':
#         return {}
#     return {'start': request.node.rep_setup.start, 'stop': request.node.rep_setup.stop}
