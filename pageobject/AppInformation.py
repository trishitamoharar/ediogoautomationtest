import time
from pageobject.Help import Help


class AppInformation:

    def __init__(self, driver):
        self.driver = driver
        

    def privacy_policy_text(self):
        self.driver.find_element_by_xpath(
            "//android.view.ViewGroup[@content-desc='privacyPolicyText']/android.widget.TextView").click()

    def click_more_tab(self):
        self.driver.find_element_by_accessibility_id("More, tab, 4 of 4").click()

    def scrollAndClick(self, visibleText):
        self.driver.find_element_by_android_uiautomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + visibleText + "\").instance(0))")

    def case161(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(3)
        # self.driver.find_element_by_xpath(
        #     "//android.view.ViewGroup[@content-desc='backfromsecurity']/android.widget.ImageView").click()
        self.scrollAndClick("Privacy Policy")
        time.sleep(5)
        self.privacy_policy_text()
        # time.sleep(25)
        # # urlactual = self.driver.current_url()
        # # urlexpected = "https://ccaeducate.me/about-cca/policies/privacy-policy/"
        # # assert urlactual == urlexpected


    def case162(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(2)
        # self.driver.find_element_by_xpath(
        #     "//android.view.ViewGroup[@content-desc='backfromsecurity']/android.widget.ImageView").click()
        self.scrollAndClick("Privacy Policy")
        time.sleep(5)
        self.privacy_policy_text()
        time.sleep(5)
        self.driver.back()
        time.sleep(10)
        expected = self.driver.find_element_by_xpath(
            "//android.view.ViewGroup[@content-desc='privacyPolicyText']/android.widget.TextView").text
        actual = "Privacy Policy"
        assert expected == actual
