import time

import pytest
from selenium.webdriver.common.by import By

from pageobject.Help import Help
from pageobject.Dashboard import Dashboard


class More:

    def __init__(self, driver):
        self.driver = driver

    def click_overdue_item(self):
        self.driver.find_element_by_xpath(
            "//android.view.ViewGroup[@content-desc='overdueItemsMenu']/android.widget.TextView").click()

    def click_more_tab(self):
        self.driver.find_element_by_accessibility_id("More, tab, 4 of 4").click()

    ######Student having no overdue items#######

    def case53(self):
        time.sleep(5)
        time.sleep(5)
        print("user is in the dashboard page")
        text = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView").text
        textlist = text.split(" ")
        overdueitemcount = int(textlist[0])
        print(overdueitemcount)
        self.click_more_tab()
        time.sleep(3)
        # self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='backfromsecurity']/android.widget.ImageView").click()
        # time.sleep(5)
        self.click_overdue_item()
        print("The overdue item is present")
        time.sleep(4)
        actualtext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView").text
        expectedtext = "Overdue Items"
        assert actualtext == expectedtext
        print("The overdue page is available")
        self.driver.find_element_by_xpath("//android.widget.ScrollView[@content-desc='OverdueStudentList']").click()
        print("Student dropdown is present")
        time.sleep(3)
        actualname = self.driver.find_element_by_xpath(
            "//android.widget.ScrollView[@content-desc='OverdueStudentList']/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]").text
        # expectedname = "Numayr Mcphaul"
        # assert actualname == expectedname
        print("The student name is visible on clicking dropdown")
        time.sleep(3)
        self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView").click()
        time.sleep(2)
        dropdowntext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView").text
        # assert dropdowntext == expectedname
        print("The student name is present in the dropdown")
        time.sleep(5)
        self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView").click()
        time.sleep(3)
        if overdueitemcount == 0:
            expectedmessage = "No overdues available for student"
            actualmessage = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView").text
            assert expectedmessage == actualmessage
            print("message correctly displayed for no overdue items")
        else:
            print("The user has overdue items")

    def case174(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(2)
        actualUnexecusedAbsences = "Unexcused Absences"
        expectedUnexecusedAbsences = self.driver.find_element_by_xpath(
            "//android.view.ViewGroup[@content-desc='unexcusedAbsencesMenu']/android.widget.TextView[1]").text
        assert actualUnexecusedAbsences == expectedUnexecusedAbsences
        print("unexcused absence is visible")
        overdueexpected = "Overdue Items"
        overdueactual = self.driver.find_element_by_xpath(
            "//android.view.ViewGroup[@content-desc='overdueItemsMenu']/android.widget.TextView").text
        assert overdueexpected == overdueactual
        print("overdue item is visible")
        gradebookactual = self.driver.find_element_by_xpath(
            "//android.view.ViewGroup[@content-desc='gradebookItemsMenu']/android.widget.TextView").text
        gradebookexpected = "Gradebook"
        assert gradebookactual == gradebookexpected
        print("gradebook is visible")
        securityactual = self.driver.find_element_by_xpath(
            "(//android.view.ViewGroup[@content-desc='securityMenu'])[2]/android.widget.TextView").text
        securityexpected = "Security"
        assert securityactual == securityexpected
        print("Security is present")
        helpactual = self.driver.find_element_by_xpath(
            "//android.view.ViewGroup[@content-desc='helpMenu']/android.widget.TextView").text
        helpexpected = "Help"
        assert helpactual == helpexpected
        print("Help is present")
        givefeedbackactual = self.driver.find_element_by_xpath(
            "//android.view.ViewGroup[@content-desc='giveFeedbackMenu']/android.widget.TextView").text
        givefeedbackexpected = "Give Feedback"
        assert givefeedbackactual == givefeedbackexpected
        print("give feedback is present")
        logoutactual = self.driver.find_element_by_xpath(
            "//android.view.ViewGroup[@content-desc='logoutMenu']/android.widget.TextView").text
        logoutexpected = "Logout"
        assert logoutactual == logoutexpected
        print("logout is present")
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='logoutMenu']/android.widget.TextView").click()
        time.sleep(2)
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='logoutsuccessPopup']").click()
        time.sleep(5)
        self.driver.find_element_by_xpath("//android.widget.EditText[@content-desc='usernameinput']").send_keys("cgordonmcphaul")
        print("The logout functionality is working properly")

    def case176(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(4)
        self.driver.find_element_by_xpath("(//android.view.ViewGroup[@content-desc='securityMenu'])[2]").click()
        print("user clicked on the security tab")
        time.sleep(2)
        actualtext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView").text
        expectedtext = "Security"
        assert actualtext == expectedtext
        print("The security page is available")
        self.click_more_tab()
        time.sleep(4)
        actualtext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedtext = "More"
        assert actualtext == expectedtext
        print("user is back in the more page")

    def case185(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(3)
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='helpMenu']/android.widget.ImageView")
        print("The help icon is present")

    def case327(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(2)
        actualtext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedtext = "More"
        assert actualtext == expectedtext
        print("user is back in the more page")
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='unexcusedAbsencesMenu']").click()
        time.sleep(4)
        expectedtext1 = "Unexcused Absences"
        actualtext1 = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
        assert expectedtext1 == actualtext1
        print("The user is in the unexcused absence page")
        time.sleep(3)
        self.driver.find_element_by_xpath("//android.widget.Button[@content-desc='Dashboard, tab, 1 of 4']").click()
        print("user clicked on dashboard button")
        time.sleep(5)
        actualtext2 = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView").text
        exoectedtext2 = "My Students"
        assert actualtext2 == exoectedtext2
        print("The user is back to the dashboard page")
        self.click_more_tab()
        time.sleep(2)
        actualtext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedtext = "More"
        assert actualtext == expectedtext
        print("user is back in the more page")
        time.sleep(3)
        self.click_overdue_item()
        print("user clicked on the overdueitems tab")
        time.sleep(3)
        actualoverduetext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView").text
        expectedoverduetext = "Overdue Items"
        assert actualoverduetext == expectedoverduetext
        print("user is in the overdue items page")
        self.click_more_tab()
        print("user clicked on the more tab")
        time.sleep(2)
        actualtext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedtext = "More"
        assert actualtext == expectedtext
        print("user is back in the more page")

    def case329(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(2)
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='helpMenu']").click()
        print("user clicked on help menu")
        time.sleep(4)
        actualhelptext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedhelptext = "Help"
        assert actualhelptext == expectedhelptext
        print("The user is in the help page")
        self.driver.find_element_by_xpath("//android.widget.Button[@content-desc='Notifications, tab, 3 of 4']").click()
        print("user clicked on the notification tab")
        time.sleep(3)
        actualnotifictext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView").text
        expectednotifictext = "My Notifications"
        actualnotifictext == expectednotifictext
        print("user is in the notification page")
        self.click_more_tab()
        print("user clicked on the more tab")
        time.sleep(2)
        actualtext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedtext = "More"
        assert actualtext == expectedtext
        print("user is back in the more page")

    def case330(self):
        time.sleep(5)
        self.driver.find_element_by_xpath("//android.widget.Button[@content-desc='Calendar, tab, 2 of 4']").click()
        print("user clicked on calender tab")
        time.sleep(3)
        actualcaltext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedcaltext = "Calendar"
        assert actualcaltext == expectedcaltext
        print("user is in the calender page")
        self.driver.find_element_by_xpath("//android.widget.Button[@content-desc='Notifications, tab, 3 of 4']").click()
        print("user clicked on notification tab")
        time.sleep(3)
        actualnotifictext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView").text
        expectednotifictext = "My Notifications"
        actualnotifictext == expectednotifictext
        print("user is in the notification page")
        self.click_more_tab()
        print("user clicked on the more tab")
        time.sleep(2)
        actualtext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedtext = "More"
        assert actualtext == expectedtext
        print("user is back in the more page")









