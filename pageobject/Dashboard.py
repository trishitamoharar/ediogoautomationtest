import os
import time
from logging import exception
import unittest

import pytest
import xlrd
from selenium.common.exceptions import NoSuchElementException, ElementNotVisibleException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.file_detector import LocalFileDetector
from appium import webdriver
from utilities import Baseclass


class Dashboard:

    def __init__(self, driver):
        self.driver = driver

    def dashboardbutton_click(self):
        self.driver.find_element_by_xpath("//android.widget.Button[@content-desc='Dashboard, tab, 1 of 4']").click()

    def scrollAndClick(self,visibleText):
        self.driver.find_element_by_android_uiautomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+visibleText+"\").instance(0))")

    # learninglink = (By.XPATH, "//*[@class='c-navigation-wrapper']//ul/li[2]/a")

    # driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ImageView").click()

    def case8(self):
        time.sleep(7)
        # self.driver.find_element_by_xpath("(//android.widget.ImageView[@index='1'])[2]").click()
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='Overall CompletionBROOKELYN GOLDEN']/android.widget.TextView").click()
        # logger.info("clicking on overall completion tooltip for overall completion to show message")
        ##### validation of the message appearing#####
        time.sleep(5)
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='Overall AttendenceBROOKELYN GOLDEN']/android.widget.TextView").click()
        # logger.info("clicking on overall attendance tooltip and check for the overall completion message to disappear")
        #####validation for message disappear###
        time.sleep(5)
        

    def case9(self):
        time.sleep(5)
        count = len(self.driver.find_elements_by_xpath("//android.widget.ImageView[@index='1']"))
        print(count)
        time.sleep(4)
        for i in self.driver.find_elements_by_xpath("//android.widget.ImageView[@index='1']"):
            i.click()
        

    def case10(self):
        time.sleep(5)
        # driver.find_element_by_xpath("(//android.widget.ImageView[@index='1'])[2]").click()
        element = self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='Overall AttendenceBROOKELYN GOLDEN']/android.widget.TextView")
        actions = ActionChains(self.driver)
        actions.double_click(element)
        time.sleep(3)
        # validate that tooltip message not appear
        element2 = self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='Overall CompletionBROOKELYN GOLDEN']/android.widget.TextView")
        actions.double_click(element2)
        

    def case11(self):
        time.sleep(5)
        self.driver.find_element_by_accessibility_id("scrollUpArrow").click()
        time.sleep(5)
        stu = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.TextView[2]").text
        stuname = stu.upper()
        print(stuname)
        teachername = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]").text
        cousedetailsexpected = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
        CurrentGradeexpected = "Current Grade"
        ProjectedGradeexpected = "Projected Grade"
        Overdueexpected = "Overdue"

        cousedetailsactual = self.driver.find_element_by_accessibility_id("courseNameundefined0"+cousedetailsexpected).text
        CurrentGradezctual = self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='currentGrade"+cousedetailsexpected+stuname+"']/android.widget.TextView").text
        ProjectedGradeactual = self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='projectedGrade"+cousedetailsexpected+stuname+"']/android.widget.TextView").text
        Overdueactual = self.driver.find_element_by_xpath("//android.widget.TextView[@content-desc='overDue"+teachername+stuname+"']").text
        print(cousedetailsactual)
        print(CurrentGradezctual)
        print(ProjectedGradeactual)
        print(Overdueactual)
        assert cousedetailsexpected in cousedetailsactual
        assert "Current" in CurrentGradezctual
        assert "Projected" in ProjectedGradeactual
        assert Overdueexpected in Overdueactual
        ##4.Click on the Collapase button present at the any of the student and observe the functionality.
        


    def case23(self):
        time.sleep(5)
        self.driver.find_element_by_accessibility_id("Notifications, tab, 3 of 4").click()
        time.sleep(2)
        self.driver.find_element_by_accessibility_id("alertTab").click()
        time.sleep(2)
        count = len(self.driver.find_elements_by_xpath("//android.view.ViewGroup[@content-desc='alertTab']/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup"))
        print("The number of alerts present " + str(count))


    def case192(self):
        time.sleep(5)
        self.driver.find_element_by_accessibility_id("More, tab, 4 of 4").click()
        time.sleep(2)
        self.driver.find_element_by_xpath("//android.widget.Button[@content-desc='Dashboard, tab, 1 of 4']").click()
        print("user clicked on the dashboard button")
        time.sleep(3)
        actualtext2 = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView").text
        exoectedtext2 = "My Students"
        assert actualtext2 == exoectedtext2
        print("The user is back to the dashboard page")



    def case194(self):
        time.sleep(5)
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='scrollUp']").click()
        time.sleep(5)
        stu = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.TextView[2]").text
        stuname = stu.upper()
        cousenameexpected = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
        teacherassignedexpected = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]").text
        ProjectedGradeexpected = "Projected Grade"
        CurrentGradeexpected = "Current Grade"

        cousenamesactual = self.driver.find_element_by_accessibility_id("courseNameundefined0" + cousenameexpected).text
        CurrentGradezctual = self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='currentGrade" + cousenameexpected + stuname + "']/android.widget.TextView").text
        ProjectedGradeactual = self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='projectedGrade" + cousenameexpected + stuname + "']/android.widget.TextView").text
        teacherassignedactual = self.driver.find_element_by_xpath("(//android.view.ViewGroup[@content-desc='NaN" + teacherassignedexpected + "'])[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]").text

        print(cousenamesactual)
        print(CurrentGradezctual)
        print(ProjectedGradeactual)
        print(teacherassignedactual)
        assert cousenameexpected in cousenamesactual
        assert "Current" in CurrentGradezctual
        assert "Projected" in ProjectedGradeactual
        assert teacherassignedexpected in teacherassignedactual
        

    def case195(self):
        time.sleep(6)
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='scrollUp']").click()
        time.sleep(5)
        stu = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.TextView[2]").text
        stuname = stu.upper()
        cousenameexpected = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
        time.sleep(5)
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='currentGrade"+cousenameexpected+stuname+"']/android.widget.TextView").click()
        time.sleep(3)
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='projectedGrade"+cousenameexpected+stuname+"']/android.widget.TextView").click()
        



    def case12(self):
        time.sleep(6)
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='scrollUp']").click()
        time.sleep(5)
        self.scrollAndClick("Kira Golden")
        time.sleep(4)
        value = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.TextView[2]").text
        assert "Kira Golden" == value,"The scroll feature is not working correctly"
        time.sleep(3)
        print("The scroll feature is working fine in dashboard page for student")






