import datetime
import time

import pytest
from selenium.webdriver.common.by import By

from pageobject.Help import Help
from pageobject.Dashboard import Dashboard


class UI_Updates:

    def __init__(self, driver):
        self.driver = driver

    def click_overdue_item(self):
        self.driver.find_element_by_xpath(
            "//android.view.ViewGroup[@content-desc='overdueItemsMenu']/android.widget.TextView").click()

    def click_more_tab(self):
        self.driver.find_element_by_accessibility_id("More, tab, 4 of 4").click()

    def click_calander(self):
        self.driver.find_element_by_xpath("//android.widget.Button[@content-desc='Calendar, tab, 2 of 4']").click()





    def case335(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(3)
        self.click_overdue_item()
        print("The overdue item is present")
        time.sleep(4)
        actualtext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView").text
        expectedtext = "Overdue Items"
        assert actualtext == expectedtext
        print("The overdue page is available")
        self.driver.find_element_by_xpath("//android.widget.ScrollView[@content-desc='OverdueStudentList']/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup").click()
        print("user clicked on the student name")
        time.sleep(6)
        expectedtext = "Overdue Items"
        actualtext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
        assert expectedtext == actualtext
        print("The overdue page for the student is displayed")

    def case317(self):
        time.sleep(5)
        self.click_calander()
        print("calender tab is available and user clicked on calender tab")
        time.sleep(3)
        actualcaltext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedcaltext = "Calendar"
        assert actualcaltext == expectedcaltext
        print("user is in the calender page")
        firstselector = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]")
        print("The first selector is available")
        caretakerimage = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ImageView")
        print("caretaker profile picture is present")
        actualtext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView").text
        expectedtext = "My Calendar"
        assert actualtext == expectedtext
        print("selector name is My Calender for caretaker")
        headertextactual = self.driver.find_element_by_xpath("//android.widget.TextView[@content-desc='caretakerStudentListStudent']").text
        headertextexpected = "Students"
        assert headertextactual == headertextexpected
        print("students header is present")
        length = len(self.driver.find_elements_by_xpath("//android.widget.ScrollView[@content-desc='caretakerStudentListFlatList']/android.view.ViewGroup/android.view.ViewGroup"))
        print("{}{}".format("The number of students for the caretaker is ", length))
        #imageitems = self.driver.find_elements_by_xpath("//android.widget.ScrollView[@content-desc="caretakerStudentListFlatList"]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup")
        items = self.driver.find_elements_by_xpath("//android.widget.ScrollView[@content-desc='caretakerStudentListFlatList']/android.view.ViewGroup/android.view.ViewGroup")
        for i in items:
            i.find_element_by_xpath("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView")
            print("Image for student is present")
            stuname = i.find_element_by_xpath("//android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]").text
            print("student name " +stuname+ " is available")
            stugrade = i.find_element_by_xpath("//android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
            print("student grade " + stugrade + " is available")



    def case318(self):
        time.sleep(5)
        self.click_calander()
        print("calender tab is available and user clicked on calender tab")
        time.sleep(3)
        actualcaltext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedcaltext = "Calendar"
        assert actualcaltext == expectedcaltext
        print("user is in the calender page")
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup").click()
        time.sleep(7)
        print("user is in the caretaker calender page")
        x = datetime.datetime.now()
        expectedval = x.strftime("%A %#d %B %Y")
        print(expectedval)
        stringval = "//android.widget.Button[@content-desc='today " + expectedval + " selected You have no entries for this day ']/android.widget.TextView"
        print(stringval)
        self.driver.find_element_by_xpath(stringval)
        print("Today's date is showing correctly")

    def case319(self):
        time.sleep(5)
        self.click_calander()
        print("calender tab is available and user clicked on calender tab")
        time.sleep(3)
        actualcaltext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedcaltext = "Calendar"
        assert actualcaltext == expectedcaltext
        print("user is in the calender page")
        self.driver.find_element_by_xpath("//android.widget.ScrollView[@content-desc='caretakerStudentListFlatList']/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup").click()
        time.sleep(7)
        print("user is in the student calender page")
        x = datetime.datetime.now()
        expectedval = x.strftime("%A %#d %B %Y")
        print(expectedval)
        stringval = "//android.widget.Button[@content-desc='today " + expectedval + " selected You have no entries for this day ']/android.widget.TextView"
        print(stringval)
        self.driver.find_element_by_xpath(stringval)
        print("Today's date is showing correctly")

    def case320(self):
        time.sleep(5)
        self.click_calander()
        print("calender tab is available and user clicked on calender tab")
        time.sleep(3)
        actualcaltext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedcaltext = "Calendar"
        assert actualcaltext == expectedcaltext
        print("user is in the calender page")
        self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup").click()
        time.sleep(7)
        print("user is in the caretaker calender page")
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]").click()
        time.sleep(3)
        print("user clicked on the dropdown")





    def case321(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(3)
        self.click_overdue_item()
        print("user clicked on the overdue items tab")
        time.sleep(4)
        actualtext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView").text
        expectedtext = "Overdue Items"
        assert actualtext == expectedtext
        print("The overdue selector page is available")
        items = self.driver.find_elements_by_xpath("//android.widget.ScrollView[@content-desc='OverdueStudentList']/android.view.ViewGroup/android.view.ViewGroup")
        count = len(items)
        print("{}{}".format("The number of students for the caretaker is ", count))
        for i in items:
            i.find_element_by_xpath("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView")
            print("Image for student is present")
            stuname = i.find_element_by_xpath("//android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]").text
            print("student name " + stuname + " is available")
            stugrade = i.find_element_by_xpath("//android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
            print("student grade " + stugrade + " is available")
        self.driver.find_element_by_xpath("//android.widget.ImageView[@content-desc='Overdue ItemsbackImage']").click()
        time.sleep(4)
        print("user clicked on the back button")
        actualtext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedtext = "More"
        assert actualtext == expectedtext
        print("user is back in the more page")


    def case324(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(3)
        self.click_overdue_item()
        print("user clicked on the overdue items tab")
        time.sleep(4)
        actualtext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView").text
        expectedtext = "Overdue Items"
        assert actualtext == expectedtext
        print("user is in the overdue items page")
        item = self.driver.find_element_by_xpath("//android.widget.ScrollView[@content-desc='OverdueStudentList']/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup")
        expectedtext = item.find_element_by_xpath("//android.view.ViewGroup/android.widget.TextView[2]").text
        item.click()
        print("user clicked on a student tab")
        time.sleep(3)
        actualtext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView").text
        assert expectedtext == actualtext
        print("The clicked student name is displayed in the dropdown")
        expectedtext = "Please contact your student’s homeroom teacher to create a catch-up plan"
        actualtext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]").text
        assert expectedtext == actualtext
        print("The catchup plan message is displayed correctly")


