import time


class Feedback:

    def __init__(self, driver):
        self.driver = driver

    def click_more_tab(self):
        self.driver.find_element_by_accessibility_id("More, tab, 4 of 4").click()
        time.sleep(3)


    def click_feedback(self):
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='giveFeedbackMenu']").click()
        time.sleep(3)

    def click_feedback_ok(self):
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='feedbackOkButton']").click()

    def case82(self):
        time.sleep(3)
        self.click_more_tab()
        print("user clicked on the more tab")
        time.sleep(3)
        self.click_feedback()
        print("user clicked on the Give feedback option")
        time.sleep(4)
        actual3 = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]").text
        expected3 = "You will be redirected to an external survey form."
        assert actual3 == expected3
        print("The popup has appeared")
        self.click_feedback_ok()
        print("user clicked on the OK button")
        time.sleep(25)
        expected = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View[1]/android.view.View[1]/android.view.View[2]").text
        actual = "CCA edioGo Beta test-2 feedback"
        assert expected == actual
        print("user is redirected to feedback page")
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View[1]/android.widget.ListView/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View[1]/android.widget.RadioButton").click()
        print("user clicked on the Give Feedback radio button")
        time.sleep(2)
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View[1]/android.widget.Button").click()
        print("User clicked on the next button")
        time.sleep(15)
        actual2 = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View[1]/android.widget.ListView/android.view.View[1]/android.view.View").text
        expected2 = "CCA edioGo Beta test feedback"
        assert actual2 == expected2
        self.driver.back()
        time.sleep(2)
        self.driver.back()
        print("user clicked on back button twice")
        actualtext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedtext = "More"
        assert actualtext == expectedtext
        print("user is back in the more page")




