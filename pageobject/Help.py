import time

import pytest


class Help:

    def __init__(self, driver):
        self.driver = driver

    def click_more_tab(self):
        self.driver.find_element_by_accessibility_id("More, tab, 4 of 4").click()

    def click_helpmenu(self):
        self.driver.find_element_by_accessibility_id("helpMenu").click()



    def case202(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(2)
        # self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='backfromsecurity']/android.widget.ImageView").click()
        # time.sleep(5)

        element = self.driver.find_element_by_accessibility_id("helpMenu")
        bst = element.get_attribute("clickable")
        assert bst == "true"

    def case203(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(8)
        # self.driver.find_element_by_xpath(
        #     "//android.view.ViewGroup[@content-desc='backfromsecurity']/android.widget.ImageView").click()
        # time.sleep(5)
        actuallist = ['Help' ,'Contact Support', 'You can contact us between -']
        self.click_helpmenu()
        time.sleep(3)
        mylist = self.driver.find_elements_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
        newlist = []
        count = len(mylist)
        for i in mylist:
            newlist.append(i.text)
        print(newlist)
        print(actuallist)
        assert newlist == actuallist


    def case206(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(2)
        print("user clicked on more tab")
        # self.driver.find_element_by_xpath(
        #     "//android.view.ViewGroup[@content-desc='backfromsecurity']/android.widget.ImageView").click()
        # time.sleep(5)
        self.click_helpmenu()
        print("user clicked on help menu")
        self.click_more_tab()
        time.sleep(4)
        actualtext = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedtext = "More"
        assert actualtext == expectedtext
        print("user is back in the more page")



    def case205(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(2)
        # self.driver.find_element_by_xpath(
        #     "//android.view.ViewGroup[@content-desc='backfromsecurity']/android.widget.ImageView").click()
        # time.sleep(5)
        self.click_helpmenu()
        time.sleep(5)
        expecteddata = "Contact Support"
        actualdata = self.driver.find_element_by_android_uiautomator('text("Contact Support")').text
        time.sleep(3)
        assert actualdata == expecteddata

    def case204(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(2)
        # self.driver.find_element_by_xpath(
        #     "//android.view.ViewGroup[@content-desc='backfromsecurity']/android.widget.ImageView").click()
        # time.sleep(5)
        self.click_helpmenu()
        time.sleep(5)
        actualvalue = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView[1]").text
        expectedvalue = "Technical Support"
        assert actualvalue == expectedvalue

    def case192(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(2)
        self.click_helpmenu()
        time.sleep(2)
        actual = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView[3]").text
        expected = "State Testing"
        assert actual == expected
        print("State Testing is present")
        actual1 = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[3]/android.widget.TextView[1]").text
        expected1 = "(717) 710-3300"
        assert actual1 == expected1
        actual3 = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[3]/android.widget.TextView[2]").text
        print(actual3)
        expected3 = " (ext 3)"
        assert actual3 == expected3
        print("The number is present in correct format")

    def case280(self):
        time.sleep(5)
        self.click_more_tab()
        time.sleep(2)
        self.click_helpmenu()
        time.sleep(2)
        actual = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView[4]").text
        expected = "Department and Staff"
        assert actual == expected
        print("department and Staff is present")
        actual1 = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[4]/android.widget.TextView").text
        expected1 = "Contacts"
        assert actual1 == expected1
        print("Contacts is present below department and staff")
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[4]/android.widget.TextView").click()
        time.sleep(15)
        print("user clicked on contacts")
        actual2 = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.EditText").text
        expected2 = "https://ccaeducate.me/staff-contact-information/"
        assert actual2 == expected2
        print("The user is redirected to url : https://ccaeducate.me/staff-contact-information/")







