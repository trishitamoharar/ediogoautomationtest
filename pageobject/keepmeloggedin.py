import time
from datetime import datetime
import glob
import os
import xlrd
from appium import webdriver
import pytest


class Keepmeloggedin:

    def __init__(self, driver):
        self.driver = driver

    def case300(self):
        time.sleep(5)
        self.driver.close_app()
        self.driver.launch_app()
        time.sleep(20)
        # self.driver.find_element_by_xpath(
        #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ImageView").click()
        # time.sleep(3)
        # self.driver.find_element_by_xpath(
        #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ImageView").click()
        # time.sleep(3)
        checkvalue = self.driver.find_element_by_xpath(
            "//android.widget.CheckBox[@content-desc='keepmelogin']/android.widget.ImageView").get_attribute("checked")

        assert checkvalue == "false"

        self.driver.find_element_by_xpath("//android.widget.CheckBox[@content-desc='keepmelogin']/android.widget.ImageView").click()
        time.sleep(2)
        checkvalue = self.driver.find_element_by_xpath(
            "//android.widget.CheckBox[@content-desc='keepmelogin']/android.widget.ImageView").get_attribute("checked")

        assert checkvalue == "false"
        self.driver.find_element_by_xpath(
            "//android.widget.CheckBox[@content-desc='keepmelogin']/android.widget.ImageView").click()
        time.sleep(2)
        checkvalue = self.driver.find_element_by_xpath(
            "//android.widget.CheckBox[@content-desc='keepmelogin']/android.widget.ImageView").get_attribute("checked")

        assert checkvalue == "false"

    def case301(self):
        time.sleep(5)
        self.driver.close_app()
        self.driver.launch_app()
        time.sleep(20)
        # self.driver.find_element_by_xpath(
        #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ImageView").click()
        # time.sleep(3)
        # self.driver.find_element_by_xpath(
        #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ImageView").click()
        # time.sleep(3)
        self.driver.find_element_by_accessibility_id("usernameinput").send_keys("cgolden7773")
        self.driver.find_element_by_accessibility_id("usernameinputinput").send_keys("Passw0rd!")
        self.driver.find_element_by_accessibility_id("logintext").click()
        time.sleep(6)
        self.driver.close_app()
        self.driver.launch_app()

        time.sleep(20)
        # self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ImageView").click()
        # time.sleep(3)
        # self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ImageView").click()
        # time.sleep(3)
        checkvalue = self.driver.find_element_by_xpath("//android.widget.CheckBox[@content-desc='keepmelogin']/android.widget.ImageView").get_attribute("checked")

        assert checkvalue == "false"
        self.driver.find_element_by_xpath("//android.widget.CheckBox[@content-desc='keepmelogin']/android.widget.ImageView").click()
        time.sleep(2)
        checkvalue = self.driver.find_element_by_xpath("//android.widget.CheckBox[@content-desc='keepmelogin']/android.widget.ImageView").get_attribute("checked")

        assert checkvalue == "false"


    def case302(self):
        time.sleep(5)
        self.driver.close_app()
        self.driver.launch_app()
        time.sleep(20)
        # self.driver.find_element_by_xpath(
        #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ImageView").click()
        # time.sleep(3)
        # self.driver.find_element_by_xpath(
        #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ImageView").click()
        # time.sleep(3)
        self.driver.find_element_by_accessibility_id("usernameinput").send_keys("cgordonmcphaul3967")
        self.driver.find_element_by_accessibility_id("usernameinputinput").send_keys("Passw0rd!")
        self.driver.find_element_by_xpath(
            "//android.widget.CheckBox[@content-desc='keepmelogin']/android.widget.ImageView").click()
        self.driver.find_element_by_accessibility_id("logintext").click()
        time.sleep(4)
        self.driver.close_app()
        self.driver.launch_app()
        time.sleep(24)
        expectedvalue = "My Students"
        actualvalue= self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView").text
        assert expectedvalue == actualvalue
        print("user is in the dashboard page")

    def case303(self):
        time.sleep(5)
        self.driver.close_app()
        self.driver.launch_app()
        time.sleep(20)
        try:
            self.driver.find_element_by_accessibility_id("usernameinput").send_keys("cgordonmcphaul3967")
        except Exception as e:
            pytest.fail(e, pytrace=True)

