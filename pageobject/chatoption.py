import datetime
import time
from datetime import date

import pytest
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.common.by import By

from pageobject.Help import Help
from pageobject.Dashboard import Dashboard
from selenium import webdriver

from utilities.Testdata import TestData


class Chatoption:

    def __init__(self, driver):
        self.driver = driver

    def click_more_tab(self):
        self.driver.find_element_by_accessibility_id("More, tab, 4 of 4").click()

    def click_chat(self):
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='chatItemsMenu']").click()

    def click_helpmenu(self):
        self.driver.find_element_by_accessibility_id("helpMenu").click()

    def scrollAndClick(self, visibleText):
        self.driver.find_element_by_android_uiautomator(
            "new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + visibleText + "\").instance(0))")


    def case391(self):
        time.sleep(5)
        self.click_more_tab()
        print("user clicked on more")
        time.sleep(2)
        self.click_chat()
        print("user clicked on chat")
        time.sleep(3)
        actual = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]").text
        expected = "Chats"
        assert actual == expected
        print("user is in the chats page")
        self.driver.find_element_by_xpath("//android.widget.EditText[@content-desc='searchInGroupChat']")
        print("The searchbox is present")
        drop = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]")
        print("The dropdown is present ")
        drop.click()
        print("user clicked on dropdown")
        time.sleep(3)
        items = self.driver.find_elements_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
        print(len(items))
        for i in items:
            print(i.text)

        actualtext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView").text
        expected = "Active"
        assert actualtext == expected
        print("by default Active is selected")
        time.sleep(2)
        self.driver.find_element_by_xpath("//android.widget.ImageView[@content-desc='chatBackImage']").click()
        print("The user clicked on the back button")
        time.sleep(3)
        actualtext = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]").text
        expectedtext = "More"
        assert actualtext == expectedtext
        print("user is back in the more page")




    def case398(self):

        time.sleep(3)
        self.click_more_tab()
        print("user clicked on more")
        time.sleep(2)
        self.click_chat()
        print("user clicked on chat")
        time.sleep(6)
        name = self.driver.find_element_by_xpath("//android.widget.ScrollView[@content-desc='chatFlatList']/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]").text
        self.driver.find_element_by_xpath("//android.widget.EditText[@content-desc='searchInGroupChat']").send_keys(name)
        time.sleep(2)
        print("user searched for the name")
        self.driver.find_element_by_xpath("//android.widget.ScrollView[@content-desc='chatFlatList']/android.view.ViewGroup/android.view.ViewGroup[1]").click()
        time.sleep(2)
        print("user clicked on the searched student name")
        actual = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[1]").text
        assert actual == name
        print("user is in the chat page of the selected student")
        self.driver.find_element_by_xpath("//android.widget.EditText[@content-desc='Type a message...']").click()
        print("user clicked on the type a message area")
        time.sleep(8)
        print(self.driver.is_keyboard_shown())
        # self.driver.press_button('1')
        # self.driver.press_button('a')
        self.driver.press_keycode(29)
        self.driver.press_keycode(62)

        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='send']/android.view.ViewGroup/android.widget.Button/android.widget.TextView").click()
        print("user typed in the keyboard and sent it")
        time.sleep(2)



    def case412(self):
        time.sleep(5)
        self.click_more_tab()
        print("user clicked on the more tab")
        time.sleep(3)
        self.click_helpmenu()
        print("User clicked on the help menu")
        time.sleep(3)
        self.scrollAndClick("Key Contacts")
        print("user scrolled to Key Contacts")
        time.sleep(5)
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup").click()
        time.sleep(5)
        # actual = self.driver.find_element_by_xpath(
        #     "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[1]").text
        # assert actual == TestData.name
        # print("user is in the chat page of the selected student")
        self.driver.find_element_by_xpath("//android.widget.EditText[@content-desc='Type a message...']").click()
        print("user clicked on the type a message area")
        time.sleep(7)
        self.driver.press_keycode(29)
        self.driver.press_keycode(62)
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='send']/android.view.ViewGroup/android.widget.Button/android.widget.TextView").click()
        print("user typed in the keyboard and sent it")
        time.sleep(2)

    def case415(self):
        time.sleep(3)
        self.click_more_tab()
        print("user clicked on more")
        time.sleep(2)
        self.click_chat()
        print("user clicked on chat")
        time.sleep(7)
        self.driver.find_element_by_xpath("//android.widget.ScrollView[@content-desc='chatFlatList']/android.view.ViewGroup/android.view.ViewGroup[1]").click()
        time.sleep(2)
        print("user clicked on the student name")
        messagearea = self.driver.find_element_by_xpath("//android.widget.EditText[@content-desc='Type a message...']")
        messagearea.click()
        print("user clicked on the type a message area")
        time.sleep(8)
        print(self.driver.is_keyboard_shown())
        messagearea.send_keys("😚")
        self.driver.find_element_by_xpath("//android.view.ViewGroup[@content-desc='send']/android.view.ViewGroup/android.widget.Button/android.widget.TextView").click()
        print("user typed emoji from keyboard and sent it")




